* Should I handle the event when a user has two accounts. Say the off account uses the user's Twitter account. 
The user then tries to add his twitter under his main account. ...what happens then? Right now, it just tries to 
switch to the off account.

* Rspec

* Update Readme warnings about altered devise/shared/_links and routes.

* Give authentications the option of nicknames, for diff accounts
* auths needs tokens and secrets

* If oauth doesn't exist but the email address is on record, offer to link the account? or tell what other oauth service is linekd to the email?
* Test pass reset/emails, etc...
