BoilerPlateRails::Application.routes.draw do

  resources :examples

  # Omniauth callback routes
  match '/auth/:provider/callback' => 'authentications#create'
  match '/auth/failure', :to => 'authentications#failure'

  #resources :authentications do
    get '/authentications' => 'authentications#index'
    post '/authentications' => 'authentications#create'
    delete '/authentications/:id' => 'authentications#destroy', :as => :destroy_authentication
    get '/auth_failure' => 'authentications#failure'
  #end

  devise_for :users, :controllers => {
    :registrations => 'registrations',
    :sessions => 'sessions'
  } 
    
  devise_scope :user do
    root :to => "pages#index"

    # Standard sign up/in/out links
    get '/sign_up' => 'registrations#new'
    get '/sign_in' => 'sessions#new', :as => :new_user_session
    post '/sign_in' => 'sessions#create', :as => :user_session
    delete '/sign_out' => 'sessions#destroy', :as => :destroy_user_session
    
    # Devise utility stuff
    get '/settings' => 'registrations#edit'    
    get '/resend_unlock_email' => 'devise/unlocks#new'
    get '/resend_confirmation_email' => 'devise/confirmations#new'
    get '/reset_password' => 'devise/passwords#new'
    
    # User wants to remove oauth provider as sign_up means.
    # FIXME: should be a POST
    get '/clear_third_party' => 'registrations#clear_third_party'
    
    # User attemps to LOG IN (not sign up) with oauth provider but auth isn't found.    
    get '/no_account' => 'sessions#no_account'
  end

end