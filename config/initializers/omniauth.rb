require 'openid/store/filesystem'
Rails.application.config.middleware.use OmniAuth::Builder do

  provider :google_oauth2,
           '1011882548016.apps.googleusercontent.com',
           'aUWQIOOJn7neCVagiO1DDPD3',
           {
               :access_type => 'online',
               :approval_prompt => '',
               :client_options => {:ssl => {:ca_path => '/etc/ssl/certs'}}
           }


  provider :twitter,
           'Djgce0uPOLSc9JbBklIww',
           'FlYsgLkVN1bhcEmttsQpHDJb4JxMMeKms24FN39X7M'

  provider :facebook,
           '236514966386127',
           '01f90d67ded11c2379354525fcda20b0'

  provider :openid, :store => OpenID::Store::Filesystem.new('./tmp')


  OmniAuth.config.logger = Logger.new(STDOUT)
  OmniAuth.logger.progname = "omniauth"
end


