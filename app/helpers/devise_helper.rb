# Helper methods for Devise
module DeviseHelper

  # Overrides Devise's devise_error_messages! helper. Renders nicer, bootstrap alert styled devise errors. See:
  # {https://github.com/plataformatec/devise/blob/master/app/helpers/devise_helper.rb#L6} for the original.
  #
  # Just remove this method to revert to default method.
  def devise_error_messages!
    return "" if resource.errors.empty?

    errors_as_list_items = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join

    render(:inline=> "<div class='alert alert-error'><ul>#{errors_as_list_items}</ul></div>")
  end

end