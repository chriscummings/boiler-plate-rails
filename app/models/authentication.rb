# == Schema Information
#
# Table name: authentications
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  provider   :string(255)
#  uid        :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Authentication < ActiveRecord::Base

  # ================================================================================================================== #
  # Attributes                                                                                              Attributes #
  # ================================================================================================================== #

  attr_accessible :provider, :uid, :user_id

  # ================================================================================================================== #
  # Associations                                                                                          Associations #
  # ================================================================================================================== #

  belongs_to :user

  # ================================================================================================================== #
  # Validations                                                                                            Validations #
  # ================================================================================================================== #

  validates :provider, :presence => {:message => 'Provider cannot be blank'}
  validates :uid, :presence => {:message => 'Provider cannot be blank'}
  # NO! THIS BREAKS SHIT
  #validates :user_id, :presence => {:message => 'Provider cannot be blank'}
  
  # Ensure there are no duplicate auths. Example: there can't be two twitter auths w/same uid.
  validates_uniqueness_of :uid, :scope => [:provider]
  

  # ================================================================================================================== #
  # Hooks                                                                                                        Hooks #
  # ================================================================================================================== #

  # ================================================================================================================== #
  # Public Methods                                                                                      Public Methods #
  # ================================================================================================================== #

  # Is the the only remaining login option for a user? (the don't delete it!)
  #
  # @return [Bool]
  def only_login_option?
    user = self.user
    if user.authentications.length < 2 and not user.has_a_password?
      return true
    else
      return false
    end
  end

  # ================================================================================================================== #
  # Private Methods                                                                                    Private Methods #
  # ================================================================================================================== #
  private

end
