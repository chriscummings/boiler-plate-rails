# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string(255)
#  failed_attempts        :integer
#  unlock_token           :string(255)
#  locked_at              :datetime
#

# User Model
class User < ActiveRecord::Base

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable
  
  # ================================================================================================================== #
  # Attributes                                                                                              Attributes #
  # ================================================================================================================== #

  attr_accessible :email, :password, :password_confirmation, :remember_me,  :current_password
  attr_accessor :current_password

  # ================================================================================================================== #
  # Associations                                                                                          Associations #
  # ================================================================================================================== #

  has_many :authentications

  # ================================================================================================================== #
  # Validations                                                                                            Validations #
  # ================================================================================================================== #

  

  # ================================================================================================================== #
  # Hooks                                                                                                        Hooks #
  # ================================================================================================================== #

  # ================================================================================================================== #
  # Public Methods                                                                                      Public Methods #
  # ================================================================================================================== #

  # Check if password is set
  #
  # @return [Bool]
  def has_a_password?
    !encrypted_password.blank?
  end

  # Create a new user authentication
  #
  # @param [Hash] omniauth a hash of oauth details.
  def apply_omniauth(omniauth)
    # This differes from the RailsCast version as OpenID does NOT always provide an email it seems.
    authentications.build(:provider => omniauth['provider'], :uid => omniauth['uid'])
  end

  
  def password_required?
    (authentications.empty? || !password.blank?) && super
  end

  # Is this a new user signing up with a 3rd party oauth provider?
  #
  # @return [Bool]
  def third_party_signup?
    !password_required?
  end

  # ================================================================================================================== #
  # Private Methods                                                                                    Private Methods #
  # ================================================================================================================== #
  private

end
