# Controller for Registrations
class RegistrationsController < Devise::RegistrationsController

  # ================================================================================================================== #
  # Hooks                                                                                                        Hooks #
  # ================================================================================================================== #

  before_filter :auth_user, :only => [:update]

  # ================================================================================================================== #
  # Public Methods                                                                                      Public Methods #
  # ================================================================================================================== #

  # If half-way through the new registration process a user decides they don't want to use a particular 3rd party, this
  # will remove the omniauth data from the session object.
  def clear_third_party
    session[:omniauth] = nil
    redirect_to '/sign_up'
  end
  
  
  # Overridden 
  def new
    # Set user's intention so authentication_controller#create knows how to handle things on OAuth callback.
    session[:intention] = 'join'
    super
  end


  # Overridden 
  # Drops the omniauth data from the session object upon success?
  def create
    super
    session[:omniauth] = nil unless @user.new_record?
    session[:intention] = nil
  end


  def update
    # required for settings form to submit when password is left blank
    if params[:user][:password].blank?
      params[:user].delete("password")
      params[:user].delete("password_confirmation")
      params[:user].delete("current_password")
    end

    @user = User.find(current_user.id)

    if @user.update_attributes(params[:user])
      set_flash_message :notice, :updated
      # Sign in the user bypassing validation in case his password changed
      sign_in @user, :bypass => true
      redirect_to after_update_path_for(@user)
    else
      render "edit"
    end
  end

  # ================================================================================================================== #
  # Private Methods                                                                                    Private Methods #
  # ================================================================================================================== #
  private

  # Overridden 
  def build_resource(*args)
    super
    if session[:omniauth]
      @user.apply_omniauth(session[:omniauth])

      # FIXME: Un-commenting this causes devise errors when we do 3rd party auth and then redirect to the
      # registrations/new page.
      # Mind you, this is the first time, we haven't yet tried to save yet... Leaving this commented doesn't
      # prevent getting the errors when we try to then save.
      #@user.valid?
    end
  end


end
