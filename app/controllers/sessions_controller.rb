# Controller for Sessions
class SessionsController < Devise::SessionsController

  # ================================================================================================================== #
  # Hooks                                                                                                        Hooks #
  # ================================================================================================================== #

  # ================================================================================================================== #
  # Public Methods                                                                                      Public Methods #
  # ================================================================================================================== #



  # Overridden 
  def new
    # Set user's intention so authentication_controller#create knows how to handle things on oauth callback.
    session[:intention] = 'login'
    super
  end
  
  
  # Page for when a user tries to login with an oauth provider and the oauth authentication doesn't exist.
  def no_account
    @provider = session[:omniauth][:provider]
  end
  
  # ================================================================================================================== #
  # Private Methods                                                                                    Private Methods #
  # ================================================================================================================== #
  private

end
                                                                                                                        