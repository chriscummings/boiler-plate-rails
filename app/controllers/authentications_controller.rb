# Controller for Authentications (OAuth)
class AuthenticationsController < ApplicationController

  # ================================================================================================================== #
  # Hooks                                                                                                        Hooks #
  # ================================================================================================================== #

  before_filter :auth_user, :only => [:index, :destroy]

  # ================================================================================================================== #
  # Public Methods                                                                                      Public Methods #
  # ================================================================================================================== #


  # Show current user's authentication options.
  def index
    @authentications = current_user.authentications if current_user
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @authentications }
    end
  end


  # Handles a FAILED oauth attempt.
  def failure
    # FIXME: Deal with failure page.
  end

  # Handles a SUCCESSFUL OAuth attempt. Handles authentication and user logic.
  def create
    # Get authentication details
    omniauth = request.env['omniauth.auth']

    # Try to get an authentication record via provider and user-id
    authentication = Authentication.find_by_provider_and_uid(omniauth['provider'], omniauth['uid'])

    # If an authentication already exists, log the user in w/that auth.
    if authentication      
      sign_in_and_redirect(:user, authentication.user)
      flash[:notice] = 'Signed in'
      
    # If the user is already logged in, add the auth to his account.
    elsif current_user      
      current_user.authentications.create!(:provider => omniauth['provider'], :uid => omniauth['uid'])
      flash[:notice] = "Added authentication via #{provider_display_name omniauth[:provider]}."
      redirect_to authentications_url
      
    # If authentication is new and the user new, check user "intention" 
    else
      # Stash OAuth in session.
      session[:omniauth] = omniauth.except('extra')
      
      if session[:intention] == 'login'
        redirect_to '/no_account'
      # elsif session[:intention] == 'join'
      #   redirect_to '/sign_up'
      else
        redirect_to '/sign_up'
        #throw 'THIS SHOULD NOT HAPPEN'
      end

    end
  end


  # Destroys an authentication method.
  def destroy
    @authentication = current_user.authentications.find(params[:id])

    # Do not allow destruction of an authentication if it's the users only means of logging back in!
    if @authentication.only_login_option?
      flash[:alert] = "You can not remove your only means of authentication. "\
        "Either add another 3rd party authentication or create a password for login. "\
        "If you're tyring to delete your account, use the 'delete my account' link on the settings page."
    else
      flash[:notice] = "Removed authentication via #{provider_display_name @authentication.provider}."
      @authentication.destroy
    end

    respond_to do |format|
      format.html { redirect_to authentications_url }
      format.json { head :no_content }
    end
  end
  
  # ================================================================================================================== #
  # Private Methods                                                                                    Private Methods #
  # ================================================================================================================== #
  private 
  
end