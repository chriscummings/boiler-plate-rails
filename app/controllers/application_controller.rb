# Controler for Application
class ApplicationController < ActionController::Base
  protect_from_forgery

  # ================================================================================================================== #
  # Helper Methods  (callable from views)                                                               Helper Methods #
  # ================================================================================================================== #

  helper_method :get_session, :intention, :provider_display_name

  # For debugging.
  def get_session
    return session if Rails.env == 'development'
  end

  # For debugging.
  def intention
    return session[:intention]
  end

  # Get a presentation name for an oauth provider.
  #
  # @param [String] provider provider name
  # @return [String] oauth provider presentation name
  # @example provider_display_name('twitter') # Twitter
  def provider_display_name(provider)
    case provider
      when 'google_oauth2'
        return 'Google'
      when 'twitter'
        return 'Twitter'
      else
        return provider
    end
  end
  
  # ================================================================================================================== #
  # Public Methods                                                                                      Public Methods #
  # ================================================================================================================== #

  def auth_user
    redirect_to '/sign_in' unless user_signed_in?
  end


  

  
  




end
