boiler-plate-rails
==================

# Actions Required for a New App

* Configure action_mailer host for production in file `\config\environments\production.rb`
* Adding/removing oauth provider strategies:
    * edit links in file `\app\views\shared\_third_party_authentication_links.html.erb`
    * edit lines under `# Omniauth` in file `\Gemfile`
    * edit file `<initializer ???>`

# Warnings

* I've overridden Devise's `devise_error_messages!` helper method. For details/removal see file `\app\helpers\devise_helper.rb`.


# Rake Tasks

* `rake yard` generates YARD documentation to the `\doc` directory. Navigate to `<path to rails>\doc\index.html`.


# Included Gems

###[Devise](https://github.com/plataformatec/devise)

###[Omniauth](https://github.com/intridea/omniauth)


## Testing

###[Rspec](https://github.com/dchelimsky/rspec)
Removal:

* remove line `gem 'rspec-rails'` from file `\Gemfile`
* remove directory `\spec`
* remove file `\.rspec`


### [VCR](https://github.com/vcr/vcr)
Removal:

* remove line `gem 'vcr'` from file `\Gemfile`
* remove block `# VCR Configuration` from file `\spec\spec_helper.rb`

### [Fakeweb](https://github.com/chrisk/fakeweb)
Removal:

* remove line `gem 'fakeweb'` from file `\Gemfile`


## Deployment

### [Capistrano](https://github.com/capistrano/capistrano)

Removal:

* remove line `gem 'capistrano'` from file `\Gemfile`
* remove file `\config\deploy.rb`

## Assets

### [uglifier](https://github.com/lautis/uglifier) JavaScript compression.
Removal:

* remove line `gem 'uglifier'` from file `\Gemfile`

### [execjs](https://github.com/sstephenson/execjs)
Removal:

* remove line `gem 'execjs'` from file `\Gemfile`

### [therubyracer](https://github.com/cowboyd/therubyracer)
Removal:

* remove line `gem 'therubyracer'` from file `\Gemfile`



### [jquery-rails](https://github.com/indirect/jquery-rails)
Removal:

* remove line `gem 'jquery-rails'` from file `\Gemfile`
* remove line `//= require jquery` from file `\app\assets\javascripts\application.js`
* remove line `//= require jquery_ujs` from file `\app\assets\javascripts\application.js`

## Misc

### [YARD](https://github.com/lsegal/yard)
Removal:

* remove line `gem 'yard'` from file `\Gemfile`
* remove line `gem 'redcarpet'` from file `\Gemfile`
* remove file `\lib\taks\yard.rake`

### [Better Errors](https://github.com/charliesome/better_errors)
Removal:

* remove line `gem 'better_errors'` from file `\Gemfile`
* remove line `gem 'binding_of_caller'` from file `\Gemfile`