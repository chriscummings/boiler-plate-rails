# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string(255)
#  failed_attempts        :integer
#  unlock_token           :string(255)
#  locked_at              :datetime
#

require 'spec_helper'

describe User do
  

  
  it 'has a valid factory' do
    create(:user).should be_valid
  end
  
  it 'is invalid without an email' do
    build(:user, :email => nil).should_not be_valid
  end
  
  it 'is invalid without a password' do
    build(:user, :password => nil).should_not be_valid
  end
  
  it 'is valid without a password but with auths' do
    build(:user_with_auths, :password => nil).should be_valid
  end
  
  it 'is invalid with a bogus email' do
    build(:user, :email => 'chris@chriscummings').should_not be_valid
  end
  
  it 'is invalid with duplicate emails' do
    create(:user, :email => 'chris@chriscummings.net')
    build(:user, :email => 'chris@chriscummings.net').should_not be_valid
  end

  it 'is invalid with a short password' do
    build(:user, :password => '1').should_not be_valid
  end
  
  it 'is valid with a 6 char pass' do
    build(:user, :password => '123456').should be_valid
  end
  
  it 'has 3 authentications' do
    create(:user_with_auths).authentications.count.should eq 3
  end 
  
  describe 'has_a_password?' do
    it 'is true with a password' do
      build(:user).has_a_password?.should be_true
    end
    it 'is false without a password' do
      build(:user, :password => nil).has_a_password?.should be_false
    end    
  end
  
  describe 'third_party_signup?' do
    it 'is true: no pass, yes auths' do
      build(:user_with_auths, :password => nil).third_party_signup?.should be_true
    end

    it 'is false: yes pass, no auths' do
      build(:user).third_party_signup?.should be_false
    end
    
    it 'is false: no pass, no auths' do
      build(:user, :password => nil).third_party_signup?.should be_false
    end
    
    it 'is false: yes pass, yes auths' do
      build(:user_with_auths).third_party_signup?.should be_false
    end
    
  end
  

  
end
