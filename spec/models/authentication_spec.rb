# == Schema Information
#
# Table name: authentications
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  provider   :string(255)
#  uid        :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'spec_helper'

describe Authentication do
  
  it 'has a valid factory' do
    create(:authentication).should be_valid
  end
  
  it 'is invalud without a provider' do
    build(:authentication, :provider => nil).should_not be_valid
  end
  
  
  describe 'only_login_option?' do
    it 'is only login option' do
      user = create(:user_has_authentication_no_pass)
      auth = user.authentications.first
      auth.only_login_option?.should eq true
    end    
    
    
    it 'is not only login option' do
      auth = create(:authentication)
      auth.only_login_option?.should eq false
    end
  end
  

  
  
  it 'is invalud without a uid' do
    build(:authentication, :uid => nil).should_not be_valid
  end
  
  # Nope. Doing so would buiding authentications on new users (users not created yet)
  # it 'is invalud without a user_id' do
  
  it 'should not allow duplicate authentications' do
    create(:authentication, :provider => 'twitter', :uid => '169791147')
    # Making up a new user_id to avoid duplicate user.email validation errors
    build(:authentication, :user_id => 9001, :provider => 'twitter', :uid => '169791147').should_not be_valid
  end

end
