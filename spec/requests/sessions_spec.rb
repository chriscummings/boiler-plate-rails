require 'spec_helper'

describe "Sessions" do
  
  include RequestMacros
  
    it 'renders log in page' do
      visit '/sign_in'
      page.should have_content 'Sign in'
    end
    
    it 'logs in' do 
      user = create(:user)
      sign_in user

      current_path.should eq '/'
      page.should have_content 'Signed in successfully'
      page.should_not have_content 'Invalid email or password'
    end

end
