require 'spec_helper'

describe "Authentications" do
  
  include RequestMacros

  it 'renders authentications page' do
    user = create(:user)
    sign_in user
    visit '/authentications'
    
    page.should have_content 'You can log in to your account using:'
  end
  
  it 'removes an authentications' do
    user = create(:user)
    sign_in user
    visit '/authentications'
    
    page.should have_content 'You can log in to your account using:'
  end
  
end





