require 'spec_helper'

describe 'Devise' do
  
  include RequestMacros

  describe 'password_reset' do
    
    it 'sends a password reset email' do
      user = create(:user)
      visit '/reset_password'
      fill_in 'Email', with: user.email
      click_button 'Send me reset password instructions'
      open_last_email.should be_delivered_to user.email
    end

    it 'warns of invalid address' do
      visit '/reset_password'
      fill_in 'Email', with: 'molly@sauter.com'
      click_button 'Send me reset password instructions'
      page.should have_content 'Email not found'
    end   
     
  end




end