require 'spec_helper'

describe "Registrations" do
  
    it 'renders sign_up page' do
      visit '/sign_up'
      page.should have_content 'Create an account'
    end
    
    it 'signs up' do 
      visit '/sign_up'
      expect{
        fill_in 'Email', with: 'newuser@example.com'
        fill_in 'Password', with: 'secret123'
        fill_in 'Password confirmation', with: 'secret123'
        click_button 'Sign up'
      }.to change(User, :count).by(1)
  
      current_path.should eq '/'
      page.should have_content 'You have signed up successfully.'
    end

end