module RequestMacros
  # controller login helper omitted ...
  def sign_in(user)
    visit '/sign_in'

    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_button 'Log In'
  end 
end

