module ControllerMacros  
  
  options = {}
  
  def random_twitter_uid
    return rand(999999999) +1
  end
  
  # Custom user
  def make_user(options = {})
    
    # Check for invalid configuration
    if options[:password] == false && options[:authentications] == 0
      throw 'a user must have either a passowrd or at least 1 authentication!'
    end
    
    # Password?
    if options[:password]
      user = FactoryGirl.create(:user)
    else
      user = FactoryGirl.create(:user_has_authentication_no_pass)  
    end
    
    # Authentications?
    if options[:authentications]
      # Check if we're using user_has_authentication_no_pass which alreay has 1 auth
      if user.authentications.length == 1 && options[:authentications] > 1
        (options[:authentications] -1).times do
          user.authentications << Authentication.new(:provider => 'twitter', :uid => random_twitter_uid, :user_id => user)
        end
      else
        (options[:authentications]).times do
          user.authentications << Authentication.new(:provider => 'twitter', :uid => random_twitter_uid, :user_id => user)
        end
      end
      
      user.save
    end

    # Logged in?
    if options[:logged]
      sign_in user
    end
    
    #@user.confirm! # or set a confirmed_at inside the factory. Only necessary if you are using the confirmable module
    
    return user
  end
  


  
  # Create another user that we have access to its attrs
  def make_not_logged_in_user(options)
    
    pass  = options[:password]
    auths = options[:authentications]
    
    # Explicitly tell Devise which mapping to use
    @request.env["devise.mapping"] = Devise.mappings[:user]
    make_not_logged_in_user = FactoryGirl.create(:user)
    return make_not_logged_in_user
  end
  
  def make_google_oauth_request_variable
    request.env['omniauth.auth'] = {}
    request.env['omniauth.auth']['provider'] = 'google_oauth2'
    request.env['omniauth.auth']['uid'] = Faker::Internet.email
    @omniauth = request.env['omniauth.auth']
  end
  
  
  # def login_admin
  #   before(:each) do
  #     @request.env["devise.mapping"] = Devise.mappings[:admin]
  #     sign_in FactoryGirl.create(:admin) # Using factory girl as an example
  #   end
  # end

  # def login_user(user)
  #   before(:each) do
  #     @request.env["devise.mapping"] = Devise.mappings[:user]
  #     
  #     if !user
  #       user = FactoryGirl.create(:user)        
  #     end
  #     
  #     #user.confirm! # or set a confirmed_at inside the factory. Only necessary if you are using the confirmable module
  #     sign_in user
  #   end
  # end
  
end