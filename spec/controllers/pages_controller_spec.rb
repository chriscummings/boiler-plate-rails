require 'spec_helper'

describe PagesController do

  shared_examples('Public access to pages') do
    describe "GET 'index'" do
      it "returns http success" do
        get 'index'
        response.should be_success
      end
    end
  end
  
  shared_examples('User access to pages') do
  end


  describe 'The Public\'s access' do
    it_behaves_like "Public access to pages"
  end
  
  describe 'A User\'s access' do
    it_behaves_like "Public access to pages"
  end

end
