require 'spec_helper'
  
describe AuthenticationsController do

  include ControllerMacros

  it "has a current_user" do
    user = make_user({})
    # note the fact that I removed the "validate_session" parameter if this was a scaffold-generated controller
    user.should_not be_nil
  end

  shared_examples('Public access to authentications') do
    describe '#failure' do
      it 'gets failure page' do
        get :failure
        response.should render_template :failure
      end
    end
    
    describe '#create' do
      it 'handles an existing authentication' do
        # set up request variable for succesful Google OAuth
        make_google_oauth_request_variable
        
        # set up a matching auth and a user
        create(:authentication, :provider => @omniauth['provider'], :uid => @omniauth['uid'])
        
        post :create
        response.should redirect_to root_url
      end
      
      it 'handles a new auth for a current user' do
        # Log in
        make_user({:password => true, :authentications => 0, :logged => true})
        
        # set up request variable for succesful Google OAuth
        make_google_oauth_request_variable
        
        post :create
        response.should redirect_to '/authentications'
      end
      
      it 'handles a new auth for a new user' do
        # set up request variable for succesful Google OAuth
        make_google_oauth_request_variable
        
        post :create
        response.should redirect_to '/sign_up'
      end
      
      # This must be handled on the integration level      
      # it 'handles a failed auth attempt (#failure)' do

    end 
  end # /shared_examples('public access')

  shared_examples('User access to authentications') do
    describe "#index" do
      
      before(:each) do
        @user = make_user({:logged => true})
      end
      
      it "populates an array of authentications" do
        get :index
        assigns(:authentications).should eq @user.authentications
      end

      it 'renders the :index view' do
        get :index
        response.should render_template :index
      end
    end # /#index

    describe '#destroy' do
      it 'deletes' do
        user = make_user({:authentications => 1, :logged => true})
        
        expect{
          delete :destroy, :id => user.authentications.first
        }.to change(Authentication, :count).by(-1)
      end
      
      it 'redirects' do
        user = make_user({:authentications => 1, :logged => true})
        delete :destroy, :id => user.authentications.first
        response.should redirect_to '/authentications'
      end
      
      it 'will not delete if only means of login' do
        user = make_user({:password => false, :authentications => 1})
        expect{
          delete :destroy, :id => user.authentications.first
        }.to change(Authentication, :count).by(0)
      end
    
    end # /#destroy
  end # /shared_examples('user access')

  describe 'The Public\'s access' do
    it_behaves_like "Public access to authentications"
    
    describe '#destroy' do
      it 'requires login' do
        user = create(:user_with_auths)
        delete :destroy, :id => user.authentications.first
        response.should redirect_to '/sign_in'
      end
    end
    
    describe '#index' do
      it 'requires login' do
        get :index
        response.should redirect_to '/sign_in'
      end
    end
  end # /public access
  
  describe 'A User\'s access' do
    it_behaves_like "Public access to authentications"
    it_behaves_like "User access to authentications"
  end # /user access

end