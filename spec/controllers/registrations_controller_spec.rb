require 'spec_helper'

# Note: OAuth is handled by AuthenticationsController

describe RegistrationsController do
  
  include ControllerMacros
  
  before(:each) do
    # Explicitly tell Devise which mapping to use
    @request.env["devise.mapping"] = Devise.mappings[:user]
  end
  
  it "has a current_user" do
    user = make_user({})
    # note the fact that I removed the "validate_session" parameter if this was a scaffold-generated controller
    user.should_not be_nil
  end
  
  shared_examples('Public access to registrations') do
    
    describe '#clear_third_party' do
      it 'clears omniauth session variable' do        
        # Set up omniauth session variable
        session[:omniauth] = true
        get :clear_third_party
        session[:omniauth].should eq(nil)
      end

      it 'redirects' do
        get :clear_third_party
        response.should redirect_to :sign_up
      end
    end # /GET #clear_third_party
    
    describe "#new" do
      it "assigns a new User to @user" do
        get :new
        assigns(:user).should be_a_new(User)
      end

      it 'renders the :new template' do
        get :new
        response.should render_template :new
      end
    end # /#new
  
    describe '#create' do
      context 'valid attributes' do
        it 'saves the the user' do
          expect{
            post :create, :user => attributes_for(:user)
          }.to change(User, :count).by(1)
        end

        it 'redirects to home' do
          post :create, :user => attributes_for(:user)
          response.should redirect_to root_url
        end
      end # /valid attributes

      context 'invalid attributes' do
       it 'does not save the the user' do
         expect{
           post :create, :user => attributes_for(:user_with_invalid_attrs)
         }.to_not change(User, :count)
       end

       it 're-renderds the :new template' do
         post :create, :user => attributes_for(:user_with_invalid_attrs)
         response.should render_template :new
       end
      end # /invalid attributes
    end # /#create
  end # /shared examples('public access')

  shared_examples('User access to registrations') do
    describe '#edit' do
      
      let(:user) do
        make_user({:logged => true})
      end
      
      it "assigns the requested user to @user" do
        get :edit, :id => user
        assigns(:user).should eq user
      end

      it 'renders the :edit template' do
        get :edit, :id => user
        response.should render_template :edit
      end
    end # /#edit
    
    describe '#update' do


      
      
      before(:each) do 
        @user = make_user({:logged => true})
      end
      

      it 'finds the user' do
        put :update, :id => @user, :user => attributes_for(:user)
        assigns(:user).should eq @user
      end

      context "valid attributes" do
        it 'updates user attrs' do
          put :update, :id => @user, 
            :user => attributes_for(:user, :email => 'frodo@chriscummings.net')
          @user.reload
          @user.email.should eq 'frodo@chriscummings.net'
        end

        it 'redirects to root' do
          put :update, :id => @user, :user => attributes_for(:user)
          response.should redirect_to root_url
        end
      end # /context valid attributes

      context "invalid attributes" do
        it 'does not update' do
          put :update, :id => @user, :user => attributes_for(:user, :email => 'fuck')
          @user.reload
          @user.email.should_not eq('fuck')            
        end

        it 're-renders edit page' do
          put :update, :id => @user, :user => attributes_for(:user, :email => 'fuck')
          response.should render_template :edit
        end
      end # /context invalid attributes
    end # /#update
   
    describe '#destroy' do 
      before(:each) do
        @user = make_user({:logged => true})
      end

      it 'deletes' do
        expect{
          delete :destroy, :id => @user
        }.to change(User, :count).by(-1)
      end

      it 'redirects' do
        delete :destroy, :id => @user
        response.should redirect_to root_url
      end
    end # /#destroy
  end # /shared_example('user access')
  
  describe 'The Public\'s access' do
    it_behaves_like "Public access to registrations"
        
    describe '#edit' do
      it 'requires login' do
        get :edit
        response.should redirect_to '/sign_in'
      end
    end
    
    describe '#update' do
      it 'requires login' do
        make_user({:logged => false})
        # Try to update another user's data
        put :update, :id => @user, :user => attributes_for(:user)
        response.should redirect_to '/sign_in'
      end
    end
    
    describe '#destroy' do
      it 'requires login' do
        make_user({:logged => false})
        # Try to delete another user's data
        delete :destroy, :id => @user
        response.should redirect_to '/sign_in'
      end
    end
  end # /public access

  describe 'A User\'s access' do
    it_behaves_like "Public access to registrations"
    it_behaves_like "User access to registrations"
  end # /user access

end






















