require 'spec_helper'

# Note: OAuth is handled by AuthenticationsController

describe SessionsController do

  include ControllerMacros

  before(:each) do
    # Explicitly tell Devise which mapping to use
    @request.env["devise.mapping"] = Devise.mappings[:user]
  end

  shared_examples('Public access to sessions') do
    describe "#new" do
      it 'renders the :new template' do
        get :new
        response.should render_template :new
      end
    end

    describe '#create' do
      # Handled in integration testing
      # it 'handles failed user/pass login'
      
      # Handled in integration testing
      # it 'handles successful user/pass login'
    end
  end
  
  shared_examples('User access to sessions') do
    describe '#destroy' do 
      it 'redirects' do
        make_user({:logged => true})
        
        delete :destroy, :id => @user
        response.should redirect_to root_url
      end
    end
  end

  describe 'The Public\'s access' do
    it_behaves_like "Public access to sessions"
  end
  
  describe 'A User\'s access' do
    it_behaves_like "Public access to sessions"
    it_behaves_like "User access to sessions"
  end

end