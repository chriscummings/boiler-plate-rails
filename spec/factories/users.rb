# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string(255)
#  failed_attempts        :integer
#  unlock_token           :string(255)
#  locked_at              :datetime
#

FactoryGirl.define do
  factory :user do
    email { Faker::Internet.email } 
    password '123456'
    
    factory :user_with_invalid_attrs do
      email nil
    end
    
    factory :user_with_auths do
      after(:build) do |user|
        user.authentications << Authentication.new(:provider => 'twitter', :uid => '2424234324', :user_id => user)
        user.authentications << Authentication.new(:provider => 'google', :uid => '242423324', :user_id => user)
        user.authentications << Authentication.new(:provider => 'github', :uid => '24423324', :user_id => user)
      end
    end
    
    factory :user_has_authentication_no_pass do
      password nil
      after(:build) do |user|
        user.authentications << Authentication.new(:provider => 'twitter', :uid => '2424234324', :user_id => user)
      end
    end
    
  end
end

# FactoryGirl.define do
#   factory :user do
#     email { Faker::Internet.email } 
#     password '123456'
#     
#     factory :user_with_auths do
#       after(:build) do |user|
#         user.authentications << Authentication.new(:provider => 'twitter', :uid => '2424234324', :user_id => user)
#         user.authentications << Authentication.new(:provider => 'twitter', :uid => '242423324', :user_id => user)
#         user.authentications << Authentication.new(:provider => 'twitter', :uid => '24423324', :user_id => user)
#       end
#     end
#     
#     factory :user_with_invalid_attrs do
#       email nil
#     end
#     
#     factory :user_with_no_pass_with_auths do
#       password nil
#       
#       after(:build) do |user|
#         user.authentications << Authentication.new(:provider => 'twitter', :uid => '2424234324', :user_id => user)
#         user.authentications << Authentication.new(:provider => 'twitter', :uid => '242423324', :user_id => user)
#         user.authentications << Authentication.new(:provider => 'twitter', :uid => '24423324', :user_id => user)
#       end
#     end
#   
#   end
# end
