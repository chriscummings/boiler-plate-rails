# Yard documentation tasks.
require 'yard'
require 'yard/rake/yardoc_task'

YARD::Rake::YardocTask.new do |t|
  t.files   = ['lib/**/*.rb', 'app/**/*.rb']
  t.options = [
      #'--files', 'LICENSE',
      #'--files', 'FAQ',
      #'--title', ''
  ]
end